package io.github.helmigandi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
public class HomeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @GetMapping
    public String home(Principal principal) {
        LOGGER.info("Principal: " + principal);
        return "Welcome to Spring JWT, " + principal.getName();
    }
}
