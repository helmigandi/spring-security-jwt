package io.github.helmigandi.controller;

import io.github.helmigandi.service.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class AuthController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthController.class);

    private final TokenService tokenService;

    public AuthController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @PostMapping("/token")
    @ResponseStatus(HttpStatus.CREATED)
    public HashMap<String, String> token(Authentication authentication) {
        LOGGER.info("Authentication: {}", authentication);
        LOGGER.info("Authentication name: {}", authentication.getName());
        String token = tokenService.generateToken(authentication);
        HashMap<String, String> response = new HashMap<>();
        response.put("type", "BEARER");
        response.put("token", token);
        return response;
    }
}
