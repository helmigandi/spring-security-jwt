# Spring Security JWT

Secure your Spring Boot REST APIs with JSON Web Tokens.

## OpenSSL

1. Create private key  
   `openssl genrsa -out keypair.pem 2048`
2. Create public key  
   `openssl rsa -in keypair.pem -pubout -out public.pem`
3. Make private key encoded to pkcs8 format (private key must not commit or store privately)  
   `openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt -in keypair.pem -out private.pem`
4. Remove `keypair.pm`

## Dependencies

1. Spring Web
2. Oauth2 Resource Server (JWT Nimbus Jose)
3. Spring Configuration Processor

## HTTP API

`http POST :8080/token --auth helmi:secret -v`

```bash
POST /token HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Authorization: Basic aGVsbWk6c2VjcmV0
Connection: keep-alive
Content-Length: 0
Host: localhost:8080
User-Agent: HTTPie/2.6.0

HTTP/1.1 201
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
Connection: keep-alive
Content-Type: application/json
Date: Thu, 29 Sep 2022 02:29:54 GMT
Expires: 0
Keep-Alive: timeout=60
Pragma: no-cache
Transfer-Encoding: chunked
X-Content-Type-Options: nosniff
X-Frame-Options: DENY
X-XSS-Protection: 1; mode=block

{
   "token": "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJIZWxtaSIsInN1YiI6ImhlbG1pIiwiZXhwIjoxNjY0NDIyMTk0LCJpYXQiOjE2NjQ0MTg1OTQsInNjb3BlIjoiVVNFUjpSRUFEIFVTRVI6V1JJVEUifQ.XaA0sf0vDHPs
   UgME29M1rLeu8tQ3g-gVOv-EaZHwSxSjwschAh5H1pI8TCyT5FBZp9B9KlNYvMeJ0KbpG1jlZfg1SpB8mrr8Offc1glapLwpXeZ9O9M2cj2819Q5PgtcXVMPuuq-sxbY4py8Zlrq-MsidONbRrzT9v_vdvDwQYk9P1POLjZfpybr1g
   iJO85Rnhy4Gfcwv5dkYkjNucVGQ1gvEGM_Iwh2obBAPmAcqR7LHLbB8xkAeb0K6xOX3TxqbLxISy9LXoFL1pbnUMQ0D5ya0BuqbdmU7doyyTHPmDQwghiqwoIAu9CfvPX7u2Pf3-rFuGVsNbOCpWuA3pd5Cg",
   "type": "BEARER"
}
```

## Source

- [https://www.youtube.com/watch?v=KYNR5js2cXE&t=1944s](https://www.youtube.com/watch?v=KYNR5js2cXE&t=1944s)
